import React from 'react'
import { Provider as StateProvider } from 'react-redux'
import store from './app/redux/store'
import MainStackNavigator from './app/navigation/AppNavigator'

export default function App() {
  return (
    <StateProvider store={store}>
      <MainStackNavigator />
    </StateProvider>
  )
}